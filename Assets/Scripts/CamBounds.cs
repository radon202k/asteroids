﻿using UnityEngine;

public class CamBounds : MonoBehaviour
{
    Camera cam;

    public float LeftBound { get; set; }
    public float RightBound { get; set; }
    public float TopBound { get; set; }
    public float BottomBound { get; set; }
    public float Width { get; set; }
    public float Height { get; set; }

    void Awake()
    {
        Application.targetFrameRate = 30;

        cam = GetComponent<Camera>();

        float camExtentV = cam.orthographicSize;
        float camExtentH = (camExtentV * Screen.width) / Screen.height;
        
        LeftBound = -camExtentH;
        RightBound = camExtentH;
        BottomBound = -camExtentV;
        TopBound = camExtentV;

        Width = camExtentH * 2;
        Height = camExtentV * 2;
    }

    private void OnDrawGizmos()
    {
        // Left Bound
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(new Vector3(LeftBound, TopBound), new Vector3(LeftBound, BottomBound));

        // Right Bound
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(new Vector3(RightBound, TopBound), new Vector3(RightBound, BottomBound));

        // Top Bound
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(new Vector3(LeftBound, TopBound), new Vector3(RightBound, TopBound));

        // Bottom Bound
        Gizmos.color = Color.green;
        Gizmos.DrawLine(new Vector3(LeftBound, BottomBound), new Vector3(RightBound, BottomBound));
    }

}
