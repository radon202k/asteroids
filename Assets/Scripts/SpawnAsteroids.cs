﻿using UnityEngine;

public class SpawnAsteroids : MonoBehaviour
{
    float spawnCounter = 0f;
    float spawnDelayInSecs = 10f;

    public GameObject asteroidHuge, asteroidBig,
        asteroidMedium, asteroidMedium2,
        asteroidSmall, asteroidSmall2;
    
    public void OnReset()
    {
        Initialize();
    }

    void FixedUpdate()
    {
        spawnCounter += Time.deltaTime;
        if (spawnCounter >= spawnDelayInSecs)
        {
            spawnCounter = 0f;
            Ship ship = FindObjectOfType<Ship>();
            if (ship && ship.alive && !FindObjectOfType<GameControl>().paused)
            {
                // Top Center Area
                float xPos = Random.Range(-5, 5);
                float yPos = FindObjectOfType<CamBounds>().TopBound + 1;
                SpawnAsteroid("Big", xPos, yPos);
            }
        }
    }

    public void Initialize()
    {
        CamBounds bounds = FindObjectOfType<CamBounds>();
        
        // Top left area
        float xPos = Random.Range(bounds.LeftBound, bounds.LeftBound / 2);
        float yPos = Random.Range(bounds.TopBound / 2, bounds.TopBound);

        SpawnAsteroid("Huge", xPos, yPos);
        
        // Bottom right area
        xPos = Random.Range(bounds.RightBound / 2, bounds.RightBound);
        yPos = Random.Range(bounds.BottomBound, bounds.BottomBound / 2);

        SpawnAsteroid("Big", xPos, yPos);
        
        // Bottom left area
        xPos = Random.Range(bounds.LeftBound, bounds.LeftBound / 2);
        yPos = Random.Range(bounds.BottomBound, bounds.BottomBound / 2);

        SpawnAsteroid("Huge", xPos, yPos);
    }

    void SpawnAsteroid (string size, float xPos, float yPos)
    {
        GameObject prefab = null;
        switch (size)
        {
            case "Huge":
                prefab = asteroidHuge;
                break;
            case "Big":
                prefab = asteroidBig;
                break;
        }

        float xVel = Random.Range(-1, 1);
        float yVel = Random.Range(-1, 1);
        
        // So it doesn't spawn with almost no velocity
        float bias = 0.25f;

        Vector2 asteroidPosition = new Vector2(xPos, yPos);
        Vector2 randomVelocity = new Vector2(xVel + Mathf.Sign(xVel) * bias, yVel + Mathf.Sign(yVel) * bias);

        if (prefab)
        {
            GameObject asteroid = Instantiate(prefab, asteroidPosition, Quaternion.identity);
            asteroid.GetComponent<Asteroid>().size = size;
            asteroid.GetComponent<Asteroid>().body.velocity = randomVelocity;
            asteroid.GetComponent<Asteroid>().body.angularVelocity = Random.Range(-50, 50);
        }
    }
}
