﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public float spawnEnemyDelayInSecs = 10f;

    float counter = 0f;
    public int enemyCount = 0;
    public int enemyMax = 10;

    public GameObject enemy;
    
    void Start ()
    {
		
	}
	
	void FixedUpdate ()
    {
        if (!FindObjectOfType<GameControl>().paused)
        {
            counter += Time.deltaTime;
            if (counter >= spawnEnemyDelayInSecs && enemyCount < enemyMax)
            {
                counter = 0f;

                float leftBound = FindObjectOfType<CamBounds>().LeftBound;
                float xPos = Random.Range(leftBound - 5, leftBound);

                float bottomBound = FindObjectOfType<CamBounds>().BottomBound;
                float yPos = Random.Range(bottomBound - 5, bottomBound);

                Vector2 randomPosition = new Vector2(xPos, yPos);

                GameObject e = Instantiate(enemy, randomPosition, Quaternion.identity);
                enemyCount++;
            }
        }
	}
}
