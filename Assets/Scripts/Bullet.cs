﻿using UnityEngine;

public class Bullet : PhysicsObject
{
    float lifeTime = 4f;

    float counter = 0;

    public string identity;

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        counter += Time.deltaTime;
        if (counter >= lifeTime)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Asteroid")
        {
            bool score = identity == "Player" ? true : false;
            collision.collider.GetComponent<Asteroid>().Explode(score);
        }
        if (collision.collider.tag == "Ship")
        {
            collision.collider.GetComponent<Ship>().Die();
        }
        if (collision.collider.tag == "Enemy")
        {
            collision.collider.GetComponent<Enemy>().Explode(true);
        }

        Destroy(gameObject);
    }
}
