﻿using UnityEngine;
using System.Collections;
using TMPro;

public class GameControl : MonoBehaviour
{
    public Ship ship;

    public GameObject welcomePanel, deadPanel, pausedPanel;


    public int CurrentScore { get; set; }
    int highScore;

    public TextMeshProUGUI inGameScore, newHighScore;
    public bool paused = false;

    void OnEnable()
    {
        welcomePanel.SetActive(true);
        if (PlayerPrefs.HasKey("HighScore"))
        {
            highScore = PlayerPrefs.GetInt("HighScore");
        }
        else
        {
            highScore = 0;
            PlayerPrefs.SetInt("HighScore", highScore);
        }
    }

    void Update()
    {
        string guiText = "Points: " + CurrentScore + " HighScore: " + highScore;
        if (inGameScore.text != guiText)
        {
            inGameScore.text = guiText;
        }

        if (!welcomePanel.activeInHierarchy && !ship.alive && !deadPanel.activeInHierarchy)
        {
            OnDead();
        }

        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
                Pause();
            else
                Resume();
        }
    }

    public void StartGame()
    {
        welcomePanel.SetActive(false);

        ship.OnStart();
        GetComponent<SpawnAsteroids>().Initialize();
    }

    public void Pause()
    {
        pausedPanel.SetActive(true);
        Time.timeScale = 0;
        paused = true;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Resume()
    {
        pausedPanel.SetActive(false);
        Time.timeScale = 1;
        paused = false;
    }

    public void ResetGame()
    {
        deadPanel.SetActive(false);
        pausedPanel.SetActive(false);

        ship.gameObject.SetActive(true);
        ship.OnReset();

        Asteroid[] asteroids = FindObjectsOfType<Asteroid>();
        foreach (Asteroid a in asteroids)
        {
            a.ExplodeAndDestroy();
        }

        Bullet[] bullets = FindObjectsOfType<Bullet>();
        foreach (Bullet b in bullets)
        {
            Destroy(b.gameObject);
        }

        Enemy[] enemies = FindObjectsOfType<Enemy>();
        foreach (Enemy e in enemies)
        {
            e.Explode(false);
        }

        GetComponent<SpawnAsteroids>().OnReset();
        CurrentScore = 0;

        FindObjectOfType<SpawnEnemy>().enemyCount = 0;
        Resume();
    }

    IEnumerator DestroyIn(float secs, GameObject obj)
    {
        yield return new WaitForSeconds(secs);
        Destroy(obj);
    }

    public void OnDead()
    {
        newHighScore.gameObject.SetActive(false);

        if (PlayerPrefs.HasKey("HighScore"))
        {
            if (CurrentScore > PlayerPrefs.GetInt("HighScore"))
            {
                highScore = CurrentScore;
                PlayerPrefs.SetInt("HighScore", highScore);

                newHighScore.gameObject.SetActive(true);
            }
        }

        deadPanel.SetActive(true);
    }
}
