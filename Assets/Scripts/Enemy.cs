﻿using UnityEngine;
using TMPro;

public class Enemy : PhysicsObject
{
    Ship ship;

    float shootCounter = 0f;
    public float shootDelayInSecs = 10f;

    public GameObject bullet, explosion, pointsFeedback;
    public Transform bulletPosition;
    [Range(1f, 500f)]
    public float bulletSpeed = 250f;

    void OnEnable()
    {
        ship = FindObjectOfType<Ship>();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (ship && !FindObjectOfType<GameControl>().paused && ship.alive)
        {
            Vector2 diff = ship.body.position - body.position;

            body.AddForce(diff.normalized);

            shootCounter += Time.deltaTime;
            if (shootCounter >= shootDelayInSecs)
            {
                shootCounter = 0f;
                Shoot();
            }

            float angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 5f * Time.deltaTime);
        }
    }

    public void Explode(bool score)
    {
        if (score)
        {
            FindObjectOfType<GameControl>().CurrentScore += 500;
            Vector2 pointsFeedbackPosition = transform.position;
            pointsFeedbackPosition.y += .5f;
            GameObject pf = Instantiate(pointsFeedback, pointsFeedbackPosition, Quaternion.identity);
            pf.GetComponent<TextMeshPro>().text = "+500";
        }

        FindObjectOfType<AudioManager>().Play("Explosion");
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    void Shoot()
    {
        FindObjectOfType<AudioManager>().Play("Shoot");
        GameObject b = Instantiate(bullet, bulletPosition.position, transform.rotation);
        b.GetComponent<Bullet>().identity = "Enemy";
        b.GetComponent<Bullet>().body.freezeRotation = true;
        b.GetComponent<Bullet>().body.AddForce(transform.up * bulletSpeed);
    }

    float AngleBetween(Vector2 vector1, Vector2 vector2)
    {
        float sin = vector1.x * vector2.y - vector2.x * vector1.y;
        float cos = vector1.x * vector2.x + vector1.y * vector2.y;

        return Mathf.Atan2(sin, cos) * (180 / Mathf.PI);
    }
}
