﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PhysicsObject : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody2D body;
    protected BoxCollider2D coll;

    protected CamBounds cam;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        coll = GetComponent<BoxCollider2D>();
        cam = FindObjectOfType<CamBounds>();
    }

    protected virtual void FixedUpdate()
    {
        if (body.velocity.magnitude > 0.01f)
        {
            HandleWrapping();
        }
    }

    void HandleWrapping()
    {
        // Going up
        if (body.position.y >= cam.TopBound + coll.bounds.size.y / 3)
        {
            body.position -= new Vector2(0, cam.Height + coll.bounds.size.y * 0.8f);
        }
        // Going left
        if (body.position.x <= cam.LeftBound - coll.bounds.size.x / 3)
        {
            body.position += new Vector2(cam.Width + coll.bounds.size.x * 0.8f, 0);
        }
        // Going down
        if (body.position.y <= cam.BottomBound - coll.bounds.size.y / 3)
        {
            body.position += new Vector2(0, cam.Height + coll.bounds.size.y * 0.8f);
        }
        // Going right
        if (body.position.x >= cam.RightBound + coll.bounds.size.x / 3)
        {
            body.position -= new Vector2(cam.Width + coll.bounds.size.x * 0.8f, 0);
        }
    }
}