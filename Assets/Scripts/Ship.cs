﻿using UnityEngine;

public class Ship : PhysicsObject
{
    public bool alive = false;

    [Range(1f, 500f)]
    public float rotationSpeed = 250;

    public GameObject bullet;
    public Transform bulletPosition;
    [Range(1f, 500f)]
    public float bulletSpeed = 250f;
    
    public GameObject explosion;

    void OnEnable()
    {
    }

    void Update()
    {
        if (alive)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                body.AddForce(transform.up * 100);
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                GetComponent<Animator>().SetBool("Pressed", true);
            }

            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                GetComponent<Animator>().SetBool("Pressed", false);
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Shoot();
            }

            float horizontal = Input.GetAxis("Horizontal");
            if (Mathf.Abs(horizontal) > 0.01f)
            {
                body.MoveRotation(body.rotation - rotationSpeed * horizontal * Time.deltaTime);
            }
        }
        else
        {
            body.velocity = Vector2.zero;
            body.rotation = 0;
        }
    }

    void Shoot()
    {
        FindObjectOfType<AudioManager>().Play("Shoot");
        GameObject b = Instantiate(bullet, bulletPosition.position, transform.rotation);
        b.GetComponent<Bullet>().identity = "Player";
        b.GetComponent<Bullet>().body.freezeRotation = true;
        b.GetComponent<Bullet>().body.AddForce(transform.up * bulletSpeed);
    }

    public void OnStart()
    {
        body.MovePosition(new Vector2(0, 0));
        alive = true;
    }

    public void OnReset()
    {
        body.MovePosition(new Vector2(0, 0));
        alive = true;
    }

    public void Die()
    {
        FindObjectOfType<AudioManager>().Play("Explosion");
        alive = false;
        Instantiate(explosion, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Asteroid")
        {
            if (collision.collider.transform.position.x < cam.LeftBound
            || collision.collider.transform.position.x > cam.RightBound
            || collision.collider.transform.position.y < cam.BottomBound
            || collision.collider.transform.position.y > cam.TopBound)
                return;

            Die();
            collision.collider.GetComponent<Asteroid>().Explode(false);
        }
    }
}
 