﻿using UnityEngine;
using TMPro;

public class Asteroid : PhysicsObject
{
    SpawnAsteroids spawnAsteroids;
    GameControl game;
    public string size;

    public GameObject explosion, pointsFeedback;

    void OnEnable()
    {
        spawnAsteroids = FindObjectOfType<SpawnAsteroids>();
        game = FindObjectOfType<GameControl>();
    }

    void Update()
    {
    }

    public void ExplodeAndDestroy()
    {
        FindObjectOfType<AudioManager>().Play("Explosion");
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public void Explode(bool score)
    {
        FindObjectOfType<AudioManager>().Play("Explosion");
        Instantiate(explosion, transform.position, Quaternion.identity);

        if (size == "Huge")
        {
            if (score)
            {
                game.CurrentScore += 1;
                Vector2 pointsFeedbackPosition = transform.position;
                pointsFeedbackPosition.y += .5f;
                GameObject pf = Instantiate(pointsFeedback, pointsFeedbackPosition, Quaternion.identity);
                pf.GetComponent<TextMeshPro>().text = "+1";
            }


            for (int i = 0; i < 2; i++)
            {
                GameObject asteroid = Instantiate(spawnAsteroids.asteroidBig, transform.position, Quaternion.identity);

                float width = asteroid.GetComponent<BoxCollider2D>().bounds.size.x;

                int angle = i == 0 ? -1 : 1;
                Vector2 direction = angle * Vector2.right;
                direction.y = Random.Range(-1, 1);

                //asteroid.GetComponent<Asteroid>().body.MovePosition(body.position + direction.normalized * width / 2);
                asteroid.GetComponent<Asteroid>().body.velocity = (direction * 1f);
                asteroid.GetComponent<Asteroid>().body.angularVelocity = 10;
                asteroid.GetComponent<Asteroid>().size = "Big";
            }
        }
        if (size == "Big")
        {
            if (score)
            {
                game.CurrentScore += 10;
                Vector2 pointsFeedbackPosition = transform.position;
                pointsFeedbackPosition.y += .5f;
                GameObject pf = Instantiate(pointsFeedback, pointsFeedbackPosition, Quaternion.identity);
                pf.GetComponent<TextMeshPro>().text = "+10";
            }

            for (int i = 0; i < 2; i++)
            {
                GameObject asteroid = Instantiate(spawnAsteroids.asteroidMedium, transform.position, Quaternion.identity);

                float width = asteroid.GetComponent<BoxCollider2D>().bounds.size.x;

                int angle = i == 0 ? -1 : 1;
                Vector2 direction = angle * Vector2.right;
                direction.y = Random.Range(-1, 1);

                //asteroid.GetComponent<Asteroid>().body.MovePosition(body.position + direction.normalized * (width));
                asteroid.GetComponent<Asteroid>().body.velocity = (direction * 1.2f);
                asteroid.GetComponent<Asteroid>().body.angularVelocity = 20;
                asteroid.GetComponent<Asteroid>().size = "Medium";

            }
        }
        if (size == "Medium")
        {
            if (score)
            {
                game.CurrentScore += 50;
                Vector2 pointsFeedbackPosition = transform.position;
                pointsFeedbackPosition.y += .5f;
                GameObject pf = Instantiate(pointsFeedback, pointsFeedbackPosition, Quaternion.identity);
                pf.GetComponent<TextMeshPro>().text = "+50";
            }

            for (int i = 0; i < 2; i++)
            {
                GameObject asteroid = Instantiate(spawnAsteroids.asteroidSmall, transform.position, Quaternion.identity);

                float width = asteroid.GetComponent<BoxCollider2D>().bounds.size.x;

                int angle = i == 0 ? -1 : 1;
                Vector2 direction = angle * Vector2.right;
                direction.y = Random.Range(-1, 1);

                Vector2 deltaPos = direction.normalized * (width);
                //asteroid.GetComponent<Asteroid>().body.MovePosition(body.position + deltaPos);
                asteroid.GetComponent<Asteroid>().body.velocity = direction * 1.4f;
                asteroid.GetComponent<Asteroid>().body.angularVelocity = 30;
                asteroid.GetComponent<Asteroid>().size = "Small";

            }
        }

        if (size == "Small")
        {
            if (score)
            {
                game.CurrentScore += 100;
                Vector2 pointsFeedbackPosition = transform.position;
                pointsFeedbackPosition.y += .5f;
                GameObject pf = Instantiate(pointsFeedback, pointsFeedbackPosition, Quaternion.identity);
                pf.GetComponent<TextMeshPro>().text = "+100";
            }
        }

        Destroy(gameObject);
    }
}